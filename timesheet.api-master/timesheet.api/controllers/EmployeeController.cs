﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService employeeService;

        public EmployeeController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            //return NotFound();
            var items = this.employeeService.GetEmployees();
            return new ObjectResult(items);
        }

        [HttpGet("{id}/timesheet")]
        public IActionResult GetEmployeeTimesheet(int id)
        {
            var items = this.employeeService.GetEmployeeTimesheet(id);
            return new ObjectResult(items);
        }

        [HttpPost("updatetimesheet")]
        public IActionResult Save(object json)
        {
            try
            {
                var task = JsonConvert.DeserializeObject<List<Effort>>(json.ToString());

                employeeService.UpdateTimesheet(task);

                return Ok(new { message = "Timesheet is updated successfully." });
            }
            catch(Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}