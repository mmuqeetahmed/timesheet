﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace timesheet.data.Migrations
{
    public partial class employeeeffot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeEfforts_Days_DayId",
                table: "EmployeeEfforts");

            migrationBuilder.DropTable(
                name: "Days");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeEfforts_DayId",
                table: "EmployeeEfforts");

            migrationBuilder.DropColumn(
                name: "DayId",
                table: "EmployeeEfforts");

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "EmployeeEfforts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "EmployeeEfforts");

            migrationBuilder.AddColumn<int>(
                name: "DayId",
                table: "EmployeeEfforts",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Days",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Days", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeEfforts_DayId",
                table: "EmployeeEfforts",
                column: "DayId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeEfforts_Days_DayId",
                table: "EmployeeEfforts",
                column: "DayId",
                principalTable: "Days",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
