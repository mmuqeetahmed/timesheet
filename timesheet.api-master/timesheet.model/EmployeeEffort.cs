﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class EmployeeEffort
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public virtual Task Task { get; set; }
        public virtual Employee Employee { get; set; }
        public int Efforts { get; set; }
        public Guid GroupId { get; set; }
    }
}
