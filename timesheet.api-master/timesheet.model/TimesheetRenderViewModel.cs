﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class Effort
    {
        public Effort()
        {
            dayEfforts = new List<DayEffort>();
        }

        public int id { get; set; }
        public int employeeId { get; set; }
        public int taskId { get; set; }
        public string taskName { get; set; }
        public List<DayEffort> dayEfforts { get; set; }
        public Guid GroupId { get; set; }
        public DateTime Date { get; set; }
        public int Hour { get; set; }
    }

    public class DayEffort
    {
        public int employeeId { get; set; }
        public int taskId { get; set; }
        public DateTime date { get; set; }
        public int hours { get; set; }
    }

    public class TimesheetRenderModel
    {
        public List<Effort> ListTask { get; set; }
        public List<string> ListDateString { get; set; }
        public List<Task> TaskLookup { get; set; }
    }
}
