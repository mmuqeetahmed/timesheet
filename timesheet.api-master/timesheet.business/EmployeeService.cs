﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }

        public object GetEmployeeTimesheet(int id)
        {

            int daysToShow = 7;

            var startDate = DateTime.Now.Date;
            var endDate = DateTime.Now.Date.AddDays(daysToShow);

            var taskLookup = db.Tasks.ToList();

            List<Guid> taskRows = (from t in db.EmployeeEfforts
                               where t.Employee.Id == id && t.Date >= startDate && t.Date <= endDate
                               select t.GroupId).Distinct().ToList();

            var k = new List<string>();

            for (var i = 0; i < daysToShow; i++)
            {
                var key = startDate.AddDays(i);
                k.Add(key.ToString("yyyy-MM-dd"));
            }

            List<Effort> output = new List<Effort>();

            foreach (var guid in taskRows)
            {
                var _query = from t in db.EmployeeEfforts
                             join e in db.Employees on t.Employee.Id equals e.Id
                             join s in db.Tasks on t.Task.Id equals s.Id
                             where t.GroupId == guid
                             select new Effort()
                             {
                                 id = t.Id,
                                 employeeId = t.Employee.Id,
                                 taskId = t.Task.Id,
                                 taskName = t.Task.Name,
                                 GroupId = t.GroupId,
                                 Hour = t.Efforts,
                                 Date = t.Date
                             };

                var res = _query.ToList<Effort>();

                var item = res.FirstOrDefault();

                if (item != null)
                {
                    var _task = new Effort
                    {
                        taskId = item.taskId,
                        taskName = item.taskName,
                        GroupId = item.GroupId,
                        employeeId = item.employeeId,
                    };

                    var _dailyEfforts = new List<DayEffort>();

                    foreach (var r in res)
                    {
                        _dailyEfforts.Add(new DayEffort
                        {
                            date = r.Date,
                            hours = r.Hour,
                            employeeId = r.employeeId,
                            taskId = r.taskId
                        });
                    }

                    _task.dayEfforts.AddRange(_dailyEfforts);
                    output.Add(_task);
                }
            }

            foreach (var o in output)
            {
                foreach (var key in k)
                {
                    if (o.dayEfforts.FirstOrDefault(p => p.date.ToString("yyyy-MM-dd") == key) == null){
                        o.dayEfforts.Add(new DayEffort
                        {
                            employeeId = o.employeeId,
                            taskId = o.taskId,
                            date = Convert.ToDateTime(key),
                            hours = 0
                        });
                    };
                }
            }

            return new TimesheetRenderModel
            {
                ListDateString = k,
                ListTask = output,
                TaskLookup = taskLookup
            };

        }

        public void UpdateTimesheet(List<Effort> task)
        {
            if (task.Count > 0)
            {
                var weeklyEffortTotal = task.Sum(o => o.dayEfforts.Sum(p => p.hours));
                var weeklyEffortAvg = task.Average(o => o.dayEfforts.Average(p => p.hours));

                var t = task.FirstOrDefault();

                if (t != null)
                {
                    using (var context = db)
                    {
                        var empEntity = context.Employees.Find(t.employeeId);

                        empEntity.WeeklyEffortTotal = weeklyEffortTotal;

                        empEntity.WeeklyEffortAverage = weeklyEffortAvg;

                        db.Entry(empEntity).State = EntityState.Modified;

                        var newTasks = task.Where(o => o.id < 0).ToList();

                        if (newTasks.Count > 0)
                        {
                            foreach (var _t in newTasks)
                            {
                                var guid = Guid.NewGuid();
                                foreach (var _e in _t.dayEfforts)
                                {
                                    context.EmployeeEfforts.Add(new EmployeeEffort
                                    {
                                        GroupId = guid,
                                        Date = _e.date,
                                        Efforts = _e.hours,
                                        Employee = context.Employees.Find(_e.employeeId),
                                        Task = context.Tasks.Find(_e.taskId)
                                    });
                                }
                            }
                        }

                        var upTasks = task.Where(o => o.id >= 0).ToList();

                        if (upTasks.Count > 0)
                        {
                            foreach (var _t in upTasks)
                            {
                                var guid = _t.GroupId;
                                var empId = _t.employeeId;
                                foreach (var _e in _t.dayEfforts)
                                {
                                    var date = _e.date;
                                    var hour = _e.hours;
                                    var taskId = _e.taskId;

                                    var effort = db.EmployeeEfforts.FirstOrDefault(o => o.GroupId == guid && o.Employee.Id == empId && o.Date == date);

                                    if (effort == null)// insert 
                                    {
                                        db.EmployeeEfforts.Add(new EmployeeEffort
                                        {
                                            GroupId = guid,
                                            Date = date,
                                            Efforts = hour,
                                            Employee = db.Employees.Find(empId),
                                            Task = db.Tasks.Find(taskId)
                                        });
                                    }
                                    else// update
                                    {
                                        effort.Task = db.Tasks.Find(taskId);
                                        effort.Efforts = hour;
                                        db.Entry(effort).State = EntityState.Modified;
                                    }
                                }
                            }
                        }

                        context.SaveChanges();

                    }
                }

            }
        }

        public string GetDayName(DateTime date)
        {
            return date.ToString("dddd");
        }

    }
}