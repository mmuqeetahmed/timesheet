import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { $ } from 'protractor';

@Injectable()
export class EmployeeService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallemployees() {
        return this.http.get(this.baseapi + "/employee/getall");
    }

    getEmployeeTimesheet(id:any){
        return this.http.get(this.baseapi + "/employee/"+ id + "/timesheet");
    }

    updateTimesheet(val: any){
        return this.http.post(
            this.baseapi + "/employee/updatetimesheet", val
        );
    }

}