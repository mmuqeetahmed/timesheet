import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { $ } from 'protractor';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {

    showEmployeesList: boolean = true;

    showTimesheet: boolean = false;

    days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];;

    employees: any;

    columns: any;
    
    tasks: any;

    taskLookup: any;

    selEmployeeId: any;

    noTaskId: any =  -1;
    
    newEffortId: any = 0;

    constructor(private employeeService: EmployeeService) { }

    ngOnInit() {
        // this.employeeService.getallemployees().subscribe(data => {
        //     this.employees = data;
        // });
        this.getAllEmployees();
    }

    getAllEmployees(){
        this.employeeService.getallemployees().subscribe(data => {
            this.employees = data;
        });
    }

    onEmployeeSelected(id:any){
        this.employeeService.getEmployeeTimesheet(id).subscribe(data => {
            this.columns = data["listDateString"];
            this.tasks = data["listTask"];
            this.taskLookup = data["taskLookup"];

            this.selEmployeeId = id;
            this.showEmployeesList = false;
            this.showTimesheet = true;
        });
    }

    backToList(){
        this.getAllEmployees();
        this.showEmployeesList = true;
        this.showTimesheet = false;
    }

    addEffort(){
        this.newEffortId = this.newEffortId - 1;
        var newTask = 

            {
                "id": this.newEffortId,
                "employeeId": this.selEmployeeId,
                "taskId": this.noTaskId,
                "taskName": "",
                "dayEfforts": []
            }
        ;

        for(var i=0; i<this.columns.length; i++){
            newTask.dayEfforts.push(
                {
                    "employeeId": this.selEmployeeId,
                    "taskId": this.noTaskId,
                    "date": new Date(this.columns[i]),
                    "hours": 0
                }
            );
        }
        this.tasks.push(newTask);
    }

    onTaskChange(val: any){
        var t = this.tasks.find(x => x.id == val.id)
        debugger;
        for(var i=0; i<t.dayEfforts.length; i++){
            t.dayEfforts[i].taskId = val.taskId;
        }
    }

    updateTimesheet(){
        this.employeeService.updateTimesheet(this.tasks).subscribe(data => {
            debugger;
            //Show any messages
            alert(data["message"]);
        });
    }

    getDayName(dateString: any){
        var d = new Date(dateString);
        var dayName = this.days[d.getDay()];
        return dayName;
    }

}